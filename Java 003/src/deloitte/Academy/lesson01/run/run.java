package deloitte.Academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.Academy.lesson01.operation.OperacionesProducto;
import deloitte.Academy.lesson01.operation.Producto;
import deloitte.Academy.lesson01.enums.Categoria;

public class run {
public static final List<Producto> listaProductos= new ArrayList<Producto>();
public static final List<Producto> listaProductosVendidos= new ArrayList<Producto>();
public static void main(String[] args) {
	Producto producto1 = new Producto();
	Producto producto2 = new Producto ();
	Producto productoNuevo = new Producto();
	
	producto1.setSku("01");
	producto1.setNombre("Celular Alcatel");
	producto1.setCantidad(10);
	
	
	producto2.setSku("01");
	producto2.setNombre("Celular Alcatel");
	producto2.setCantidad(10);
	
	
	productoNuevo.setSku("01");
	productoNuevo.setNombre("Celular Alcatel");
	productoNuevo.setCantidad(10);
	
	listaProductos.add(producto1);
	listaProductos.add(producto2);
	listaProductos.add(productoNuevo);
	
	OperacionesProducto operacionesProducto = new OperacionesProducto();
	operacionesProducto.agregarProducto(productoNuevo);
	
	for (Producto prod : listaProductos) {
		System.out.println("SKU: " +prod.getSku() + "Nombre: "  + prod.getNombre() + "Cantidad " + prod.getCantidad());
	}
	operacionesProducto.venderProducto(producto2,5);
	for (Producto prod : listaProductosVendidos) {
		System.out.println("SKU: " +prod.getSku() + "Nombre: "  + prod.getNombre() + "Cantidad " + prod.getCantidad());
}
}}